# Instructions

Note: All of this can be done directly using the gitlab interface, no need to clone the repo and push commits to it.

1. Press the 'fork' button on the upper right to make a fork of this repository into your personal gitlab space (only one person needs to do this per team).

2. Open up the config file 'my_config', and fill in the secret passcode. You may find inspiration in poetry.

3. Commit the file to automatically start up the CI (it should only take a minute or so to run - if it's taking longer please let the social committe members know!!). Once the CI is finished, open up the artifact it produced. If your configuration is correct, the artifact will guide you on the next step of your journey.

